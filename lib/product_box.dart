import 'package:flutter/material.dart';
import 'product.dart';
import 'rating_box.dart';

class ProductBox extends StatelessWidget {

  ProductBox({Key? key, required this.item}) : super(key: key);
  final Product item;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(2),
      height: 140,
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset("assets/appimages/" + this.item.image),
            Expanded(child: Container(
              padding: EdgeInsets.all(5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(this.item.name,
                  style: TextStyle(fontWeight: FontWeight.bold),),
                  Text(this.item.description),
                  Text("Price: " + this.item.price.toString()),
                  RatingBox()

                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
